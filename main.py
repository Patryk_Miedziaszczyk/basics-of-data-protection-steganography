import random
import sys
import math
from builtins import object

import numpy as np
import cv2

np.set_printoptions(threshold=sys.maxsize)
image = cv2.imread(str('image.jpg'), -1)
shape = image.shape
rows = int(shape[0])
columns = int(shape[1])


def imageToBinary():
   img = np.zeros(shape=(rows, columns, 3), dtype=int)
   np.core.arrayprint._line_width = 100
   for i in range(0, rows):
       for j in range(0, columns):
           for k in range(0, 3):
               value = int(image[i][j][k])
               value = bin(value)[2:]
               img[i][j][k] = value
   return img


def imageToFile(Array, name):
   with open(str(name), 'w') as f:
       for i in range(0, rows):
           list = str(Array[i, :])
           for item in list:
               f.write("%s " % item)
           f.write('\n')


def informationToBinary(information):
   x = list(information)
   for i in range(0, len(information)):
       value = bin(ord(x[i]))[2:]
       if len(str(value)) == 1:
           value = '0000000' + str(value)
       if len(str(value)) == 2:
           value = '000000' + str(value)
       if len(str(value)) == 3:
           value = '00000' + str(value)
       if len(str(value)) == 4:
           value = '0000' + str(value)
       if len(str(value)) == 5:
           value = '000' + str(value)
       if len(str(value)) == 6:
           value = '00' + str(value)
       if len(str(value)) == 7:
           value = '0' + str(value)
       x[i] = value
   return x

def encodePassword(password, binaryImage):
   index = 0
   max = len(password)
   for i in range(0, rows):
       for j in range(0, columns):
           for k in range(0, 3):
               if index < max:
                   value = int(binaryImage[i][j][k])
                   value = list(map(int, str(value)))
                   value[-1] = password[index]
                   strings = [str(integer) for integer in value]
                   value = "".join(strings)
                   print(value)
                   binaryImage[i][j][k] = value
                   index = index + 1
   return binaryImage

def arrayToDecimal(array):
   img = np.zeros(shape=(rows, columns, 3), dtype=int)
   np.core.arrayprint._line_width = 100
   for i in range(0, rows):
       for j in range(0, columns):
           for k in range(0, 3):
               value = int(str(array[i][j][k]), 2)
               img[i][j][k] = value
   return img

def main():
   print('MENU')
   binaryImage = imageToBinary()
   imageToFile(binaryImage, 'imageBefore.txt')
   binaryPassword = informationToBinary('Tajne haslo, nie uda Ci się go znalezc')
   lengthOfBinaryPassword = len(binaryPassword) * 8
   stringPassword = ''.join(binaryPassword)
   passwordList = list(stringPassword)
   print('Password in binary: ' + str(binaryPassword))
   print('Pasword in binary string: ' + str(stringPassword))
   print('Pasword in list: ' + str(passwordList))
   print('Length of password in binary: ' + str(lengthOfBinaryPassword))
   encodeImage = encodePassword(passwordList, binaryImage)
   imageToFile(encodeImage, 'imageAfter.txt')
   imageAfterDecimal = arrayToDecimal(encodeImage)
   cv2.imwrite('result.jpg', imageAfterDecimal)
main()


